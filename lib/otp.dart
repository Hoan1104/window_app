import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pinput/pin_put/pin_put.dart';

class OTPScreen extends StatefulWidget {
  const OTPScreen({Key? key}) : super(key: key);

  @override
  _OTPScreenState createState() => _OTPScreenState();
}

class _OTPScreenState extends State<OTPScreen> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController otpController = TextEditingController();
  final FocusNode emailFocus = FocusNode();
  final FocusNode passwordFocus = FocusNode();
  final FocusNode otpFocus = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff0A1931),
      body: Center(
        child: Container(
          padding: EdgeInsets.all(16),
          width: 400,
          height: 300,
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(16)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 40,
              ),
              Text(
                'Nhập mã OTP',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.w600),
              ),
              SizedBox(
                height: 20,
              ),
              PinPut(
                withCursor: true,
                fieldsAlignment: MainAxisAlignment.spaceEvenly,
                fieldsCount: 4,
                eachFieldWidth: 50.0,
                eachFieldHeight: 50.0,
                onSubmit: (String pin) {
                  // controller.onVerifyOTP();
                },
                textStyle: TextStyle(color: Colors.black, fontSize: 17),
                focusNode: otpFocus,
                controller: otpController,
                submittedFieldDecoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(width: 2.0, color: Colors.black),
                ),
                selectedFieldDecoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(width: 2.0, color: Colors.black),
                ),
                followingFieldDecoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(width: 2.0, color: Colors.black),
                ),
                autofocus: true,
                pinAnimationType: PinAnimationType.scale,
              ),
              SizedBox(
                height: 40,
              ),
              Text(
                'Gửi lại (60)',
                style: TextStyle(color: Colors.black, fontSize: 17),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
